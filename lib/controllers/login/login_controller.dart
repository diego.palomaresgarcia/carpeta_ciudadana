import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../so_getx_controller.dart';

class LoginController extends SOGetXController {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _obscureText = false;
  late Size _size;
  double _margen = 0.0;

  GlobalKey<FormState> get formKey => _formKey;

  bool get obscureText => _obscureText;

  Size? get size => _size;

  bool get isMobileDevice => !kIsWeb && (Platform.isIOS || Platform.isAndroid);

  bool get isDesktopDevice => !kIsWeb && (Platform.isMacOS || Platform.isWindows || Platform.isLinux);

  bool get isMobileDeviceOrWeb => kIsWeb || isMobileDevice;

  bool get isDesktopDeviceOrWeb => kIsWeb || isDesktopDevice;
  double get margen => _margen;

  @override
  void onInit() {
    _size = Get.mediaQuery.size;
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onReady() {

    double width = Get.mediaQuery.size.width;
    var padding = Get.mediaQuery.padding;
    double newWidth = width - padding.left - padding.right;
    if(isMobileDevice){
      _margen = _size.width <= 400.0 ? (_size.width - 320) / 2 : 25;
    }else if(isDesktopDevice){
      _margen = (_size.width - _size.width <= 400.0 ? 25 : 400) / 2;
    }
    print("Palomares " + newWidth.toString());
    update(['vista']);
    // TODO: implement onReady
    super.onReady();
  }

  void changePasswordVisibility() {
    this._obscureText = !this.obscureText;
    update(['password']);
  }

  void verifyLogin() {}
}
