import 'dart:async';
import 'package:get/get.dart';
import 'package:meta/meta.dart' show required;
import '../model/JsonResponse.dart';
import '../model/network/network.dart';

abstract class SOGetXController extends GetxController {
  final SOControllerStatus controllerStatus = SOControllerStatus();

  Future<JsonResponse> get({@required endPoint, Map<String, dynamic> parameters = const {},parserClass}) async{
    final network = Network();
    final JsonResponse response = await network.get(endPoint: endPoint,parameters: parameters,parse: parserClass);
    return response;
  }

  Future<JsonResponse> post({@required endPoint,Map<String,dynamic> parameters = const {},parserClass}) async{
    final network = Network();
    final JsonResponse response = await network.post(endPoint: endPoint,parameters: parameters,parse: parserClass);
    return response;
  }

  Future<dynamic> put({@required endPoint,Map<String,dynamic> parameters = const {},parserClass}) async{
    final network = Network();
    final JsonResponse response = await network.put(endPoint: endPoint,parameters: parameters,parse: parserClass);
    return response;
  }

  Future<dynamic> delete({@required endPoint,Map<String,dynamic> parameters = const {},parserClass}) async{
    final network = Network();
    final JsonResponse response = JsonResponse(); //= await network.post();
    return response;
  }

  Future<dynamic> postScoring({@required url, Map<String, dynamic> parameters = const {}}) async {

    final network = Network();
    return await network.postDashboardScoring(url, params: parameters);
  }

  Future<dynamic> read({required String serviceName, Map<String, dynamic> parameters = const {}, dynamic parser}) async {

    final Network network = new Network()
      ..setRowTable(serviceName);

    parameters.forEach((key, value) {
      network.setRow(key, value);
    });

    if (parser != null) {
      network.setParser(parser);
    }

    final dynamic response = await network.read();

    return response;
  }

  Future<dynamic> write({required String serviceName, Map<String, dynamic> parameters = const {}, dynamic parser}) async {

    final Network network = new Network()
      ..setRowTable(serviceName);

    parameters.forEach((key, value) {
      network.setRow(key, value);
    });

    if (parser != null) {
      network.setParser(parser);
    }

    final dynamic response = await network.write();

    return response;
  }

  Future<dynamic> readFilter({required String serviceName, Map<String, dynamic> parameters = const {}, dynamic parser}) async {

    final Network network = new Network()
      ..setFilterTable(serviceName);

    parameters.forEach((key, value) {
      network.setFilter(key, value);
    });

    if (parser != null) {
      network.setParser(parser);
    }

    final dynamic response = await network.read();

    return response;
  }
}

class SOControllerStatus {
  String message;
  bool success;
  bool loading;

  SOControllerStatus({
    this.message: '',
    this.success: true,
    this.loading: true
  });
}