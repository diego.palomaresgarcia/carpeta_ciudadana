import 'dart:async';
import 'package:get/get.dart';
import '../../pages/login/login_page.dart';
import '../so_getx_controller.dart';

class SplashController extends SOGetXController {

  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();

    Future.delayed(Duration(seconds: 2), () {
      Get.off(LoginPage());
    });
  }
}