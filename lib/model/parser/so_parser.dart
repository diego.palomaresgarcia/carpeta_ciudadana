import 'package:xml/xml.dart';

class SOParser {

  dynamic parseJSON(Map<String, dynamic> json) {
    return json;
  }

  dynamic parse(XmlDocument document) {
    return document;
  }

  Iterable<XmlElement> getXMLRows(XmlDocument document, String tableName) {

    try {
      final Iterable<XmlElement> tables = document.findAllElements('table');
      final tabla = tables.firstWhere((tabla) => tabla.findElements('name').single.text == tableName);

      return tabla.findAllElements('row');
    }
    catch (id) {
      print(id);
      return [];
    }
  }

  int getNumberTables(XmlDocument document, String tableNameNot) {

    int total = 0;

    try {
      final Iterable<XmlElement> tables = document.findAllElements('table');
      final tabla = tables.firstWhere((tabla) => tabla.findElements('name').single.text == tableNameNot);

      tables.forEach((item)
      {
        if(item.findElements('name').single.text != tableNameNot)
        {
          total += 1;
        }
      });

      return total;
    }
    catch (id) {
      print(id);
      return 0;
    }
  }

  Iterable<XmlElement> getXMLRowsAux(XmlDocument document, String tableName) {

    try {
      var encuesta = document.findAllElements('tables');
      Iterable<XmlElement>? a;
      encuesta.forEach((item)
      {
        a = item.findAllElements('table');
      });
      return a!;
    }
    catch (id) {
      print(id);
      return [];
    }
  }

  bool getXMLStatusMessage(XmlDocument document, String tableName) {

    try {
      final Iterable<XmlElement> tables = document.findAllElements('table');
      final tabla = tables.firstWhere((tabla) => tabla.findElements('name').single.text == tableName);

      return tabla.findElements('status').single.findElements('state').single.text == '0';
    }
    catch (id) {
      print(id);
      return false;
    }
  }

  String getXMLStatusMessageText(XmlDocument document, String tableName) {

    try {
      final Iterable<XmlElement> tables = document.findAllElements('table');
      final tabla = tables.firstWhere((tabla) => tabla.findElements('name').single.text == tableName);
      return tabla.findElements('status').single.findElements('message') != null  ? tabla.findElements('status').single.findElements('message').single.text : '';
    }
    catch (id) {
      print(id);
      return "";
    }
  }

  SOStatusXML getStatus(XmlDocument document, String tableName) {
    return SOStatusXML(
        message: getXMLStatusMessageText(document, tableName),
        status: getXMLStatusMessage(document, tableName)
    );
  }
}

class SOParserItem {

  SOStatusXML status;
  dynamic data;

  SOParserItem({
    required SOStatusXML status,
    this.data
  }) : this.status = status == null ? SOStatusXML() : status;
}

class SOStatusXML {
  String message;
  final bool status;

  SOStatusXML({
    this.message: '',
    this.status: false
  });
}
