class Webservice {

  static String read = "";
  static String write = "";
  static String chat = "";
  static String news = "";
  static String base = "";
  static String performance = "";

  static String linkVersion = "";
  static String linkEnvironment = "";

  static void setup() {

    const _Environment _ENVIRONMENT = _Environment.distribution;
    const String _VERSION ="40/";

    linkEnvironment = _ENVIRONMENT.toString();
    linkVersion = _VERSION;

    switch (_ENVIRONMENT) {
      case _Environment.distribution:
        read = "https://www.sicopweb.com/xml/servicios/${_VERSION}mobile/read.xml";
        write = "https://www.sicopweb.com/xml/servicios/${_VERSION}mobile/write.xml";
        chat = "wss://dev.sicopweb.com:8000/9.0/workspace/mensajes/";
        news = "https://www.sicopweb.com/8.0/workspace/news/image.html?Webservice.news=";
        base = "https://www.sicopweb.com/xml/servicios/$_VERSION";
        performance = "https://www.sicopweb.com/";
        break;
      case _Environment.dev:
        read = "https://dev.sicopweb.com/xml/servicios/40/mobile/read.xml";
        write = "https://dev.sicopweb.com/xml/servicios/40/mobile/write.xml";
        chat = "wss://dev.sicopweb.com:8000/9.0/workspace/mensajes/";
        news = "https://dev.sicopweb.com/8.0/workspace/news/image.html?Webservice.news=";
        base = "https://dev.sicopweb.com/xml/servicios/40/";
        performance = "https://dev.sicopweb.com/";
        break;
      default:
        break;
    }
  }
}

enum _Environment {
  distribution,
  dev,
}

class XMLHeader {
  static String deviceId = "";
  static String idMarca = "";
  static String idDistribuidor = "";
  static String eMail = "";
  static String poolName = "";
  static String licencia = "";
  static String origen = "";
}