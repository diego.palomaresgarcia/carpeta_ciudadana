class JsonResponse{
  final bool success;
  final String message;
  final dynamic response;

  JsonResponse({
    this.success : false,
    this.message : '',
    this.response,
  });

  factory JsonResponse.fromJson(Map<dynamic,dynamic> json,clase){
    return JsonResponse(
      success: json['success'] ?? false,
      message: json['message'] ?? '',
      response: json.containsKey('response') ? clase.fromJson(json['response']) : null,
    );
  }
}