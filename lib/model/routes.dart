import 'package:flutter/material.dart';
import '../pages/login/login_page.dart';
import '../pages/splash/splash_page.dart';

final routes = {
  '/' : (BuildContext context) => SplashPage(),
  '/LoginPage' :(BuildContext context) => LoginPage(),
};