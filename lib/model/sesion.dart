import 'database/databaseManager.dart';

class Session {
  static Future<String> get(String sessionData) async {
    DatabaseManager db = new DatabaseManager();

    try {
      List<Map> list = await db.get('DatosDeSesion', "name = '$sessionData'");

      return list.first['value'];
    } catch (id) {
      print(id);
      return '';
    }
  }

  static Future<TokenInfo> getToken(String api) async {
    DatabaseManager db = new DatabaseManager();
    TokenInfo tokenInfo = TokenInfo();

    try {
      List<Map> list = await db.get('Token', "api = '$api'");

      if (list.isNotEmpty) {
        tokenInfo = TokenInfo(
            token: list.first['token'],
            fecha: list.first['fecha'],
            api: list.first['api']
        );
      }
    } catch (id) {
      print(id);
    }

    return tokenInfo;
  }

  static Future<String> getConfiguracion(String proceso, String variable) async {
    DatabaseManager db = new DatabaseManager();

    try {
      List<Map> list = await db.get('Configuracion', "proceso = '$proceso' AND variable = '$variable'");
      if (list.isEmpty) {
        return "";
      }
      else {
        return list.first['valor'];
      }
    } catch (id, id2) {
      print(id2);
      return '';
    }
  }

  static Future<List<Map<String, String>>> getConfiguracionAplicacion(String aplicacion) async {
    DatabaseManager db = new DatabaseManager();

    try {
      List<Map> list = await db.get('ConfiguracionAplicacion', "aplicacion = '$aplicacion'");
      List<Map<String, String>> configuraciones = [];

      list.forEach((config) {
        Map<String, String> map = new Map();
        map['campo'] = config['campo'];
        map['etiqueta'] = config['etiqueta'];
        map['requerido'] = config['requerido'];
        map['visible'] = config['visible'];
        map['defecto'] = config['defecto'];
        map['filtro'] = config['filtro'];

        configuraciones.add(map);
      });

      return configuraciones;
    } catch (id) {
      print(id);
      return [];
    }
  }

  static Future<String> getEtiqueta(String aplicacion, String campo) async {
    String etiqueta = "";
    DatabaseManager db = new DatabaseManager();

    try {
      List<Map> list = await db.get('ConfiguracionAplicacion', "aplicacion = '$aplicacion'");
      etiqueta = list.firstWhere((i) => i['campo'] == campo)['etiqueta'];
    }
    catch(_) {
      etiqueta = "";
    }

    return etiqueta;
  }

  static Future<bool> isRequired(String aplicacion, String campo) async {
    bool isRequerido = false;

    isRequerido = await Session.getConfiguracionAplicacion(aplicacion).then((value) {
      var a = value.firstWhere((test) => (test["campo"] == campo), orElse: () {
        Map<String, String> ret = new Map();
        ret["requerido"] = "0";
        return ret;
      });

      for (String key in a.keys) {
        if (a[key] == "1" && key == "requerido") {
          return true;
        }
      }

      return false;
    });

    return isRequerido;
  }

  static Future<bool> isVisible(String aplicacion, String campo) async {
    bool isVisible = false;

    isVisible = await Session.getConfiguracionAplicacion(aplicacion).then((value) {
      var a = value.firstWhere((test) => (test["campo"] == campo), orElse: () {
        Map<String, String> ret = new Map();
        ret["visible"] = "0";
        return ret;
      });

      for (String key in a.keys) {
        if (a[key] == "1" && key == "visible") {
          return true;
        }
      }

      return false;
    });

    return isVisible;
  }
}


class TokenInfo {
  final String api;
  final DateTime fecha;
  final String token;

  TokenInfo({
    this.api: '',
    String fecha: '',
    this.token: '',
  }) : this.fecha = DateTime.tryParse(fecha) ?? DateTime.now().subtract(Duration(hours: 4));
}