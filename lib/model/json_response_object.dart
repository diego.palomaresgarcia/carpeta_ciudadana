class JSONResponse {
  final bool success;
  final int status;
  final String message;
  final String token;
  final data;
  final String token_type;

  JSONResponse({
    this.message: '',
    this.status: 1,
    this.success: false,
    this.token: '',
    this.data,
    this.token_type : ''
  });

  JSONResponse.fromJson(Map<String, dynamic> response, {String dataTag: 'data'}) :
        this.message = response.containsKey('message') ? response['message'] : '',
        this.status = response.containsKey('status') ? response['status'] : 1,
        this.token = response.containsKey('token') ? response['token'] : '',
        this.success = response.containsKey('success') ? response['success'] : false,
        this.data = response.containsKey(dataTag) ? response[dataTag] : null,
        this.token_type = response.containsKey('token_type') ? response['token_type'] : '';
}