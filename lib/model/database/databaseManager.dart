import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseManager {

  static Database? _db;
  String _path = '';

  Future<Database> get db async {
    _db ??= await _initDB();
    return _db!;
  }

  Future<Database> _initDB() async {
    var databasesPath = await getDatabasesPath();
    _path = join(databasesPath, "seekop.db");

    Database database = await openDatabase(
      _path,
      version: 2,
      onCreate: (Database db, int version) async {
        await db.execute('CREATE TABLE DatosDeSesion (id INTEGER PRIMARY KEY, name TEXT, value TEXT)');
        await db.execute('CREATE TABLE Configuracion (id INTEGER PRIMARY KEY, proceso TEXT, variable TEXT, valor TEXT, editable TEXT, tipo TEXT, etiqueta TEXT)');
        await db.execute('CREATE TABLE ConfiguracionAplicacion (id INTEGER PRIMARY KEY, aplicacion TEXT, campo TEXT, etiqueta TEXT, requerido TEXT, visible TEXT, defecto TEXT, filtro TEXT)');

        if (version == 2) {
          await db.execute('CREATE TABLE Token (id INTEGER PRIMARY KEY, api TEXT, fecha TEXT, token TEXT)');
        }
      },
      onUpgrade: (Database db, versionAnterior, versionActual) async {

        if (versionActual == 2) {
          await db.execute('CREATE TABLE Token (id INTEGER PRIMARY KEY, api TEXT, fecha TEXT, token TEXT)');
        }
      },
    );

    return database;
  }

  void deleteAll() {
    deleteDatabase(_path);
  }

  Future<bool> delete(String table, {String where : ''}) async {
    Database database = await db;
    String query = 'DELETE FROM $table';

    if (where.isNotEmpty) {
      query += " WHERE $where";
    }

    int count = await database.rawDelete(query);

    if (count > 0) {
      print("deleted $count records from $table");
    }
    else {
      print("Nothing to delete from $table");
    }

    return count > 0;
  }

  Future<bool> insert(String table, List<String> keys, List<List<String>> values) async {
    bool inserted = false;
    String query = 'INSERT INTO $table ';
    String keysString = '';
    String valuesPos = '';

    keys.forEach((key) {
      keysString += '$key,';
    });

    keysString = keysString.substring(0, keysString.length - 1);

    for(int i = 0; i < values.length; i++) {
      valuesPos = '';

      values[i].forEach((value) {
        valuesPos += "'${value.replaceAll("'", '"')}',";
      });

      valuesPos = valuesPos.substring(0, valuesPos.length - 1);

      query = "INSERT INTO $table ($keysString) VALUES ($valuesPos)";

      try {
        Database database = await db;

        await database.transaction((txn) async {
          await txn.rawInsert(query);
          inserted = true;
        });
      }
      catch (id) {
        print("ERROR AL INSERTAR EN $table -> $id");
      }

    }

    return inserted;
  }

  Future<List<Map>> get(String table, String where) async {
    Database database = await db;
    String query = "SELECT * FROM $table WHERE $where";
    List<Map> list = await database.rawQuery(query);

    return list;
  }

  void test() async {
    Database database = await db;
// Update some record
    /*int count = await database.rawUpdate(
			 'UPDATE Test SET name = ?, VALUE = ? WHERE name = ?',
			 ["updated name", "9876", "some name"]);
		print("updated: $count"); */

// Count the records
    int? count = Sqflite.firstIntValue(await database.rawQuery("SELECT COUNT(*) FROM DatosDeSesion"));
    print(count);

// Delete a record
    /*count = await database
			 .rawDelete('DELETE FROM Test WHERE name = ?', ['another name']);
		assert(count == 1);*/

  }

}