import 'dart:convert';
import 'dart:typed_data';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:xml/xml.dart';
import 'dart:convert' as convert;
import '../JsonResponse.dart';
import '../constants.dart';
import '../parser/so_parser.dart';

enum _TableType { filter, row, none }

class Network {
  final String _inteface = "MX00000001";
  String _content = '';
  _TableType _type = _TableType.none;
  dynamic _parser = SOParser();

  Encoding _encoding = utf8;

  Network() {
    _setXMLHeader();
  }

  void _setXMLHeader() {
    _content = '''\n<service>\n''' + '''  <IdInterface>$_inteface</IdInterface>\n''';

    if (XMLHeader.idMarca.isNotEmpty) {
      _content += '''  <IdMarca>${XMLHeader.idMarca}</IdMarca>\n''';
    }

    if (XMLHeader.idDistribuidor.isNotEmpty) {
      _content += '''  <IdDistribuidor>${XMLHeader.idDistribuidor}</IdDistribuidor>\n''';
    }

    if (XMLHeader.eMail.isNotEmpty) {
      _content += '''  <eMail>${XMLHeader.eMail}</eMail>\n''';
    }

    if (XMLHeader.poolName.isNotEmpty) {
      _content += '''  <PoolName>${XMLHeader.poolName}</PoolName>\n''';
    }

    if (XMLHeader.deviceId.isNotEmpty) {
      _content += '''  <RegistryId>${XMLHeader.deviceId}</RegistryId>\n''';
    }

    if (XMLHeader.origen.isNotEmpty) {
      _content += '''  <Origen>${XMLHeader.origen}</Origen>\n''';
    }

    _content += '''  <Password>${_getPassword()}</Password>\n''';
    _content += '''  <tables>\n''';
  }

  void _setXMLBottom() {
    _content += '''  </tables>\n''';
    _content += '''</service>''';
  }

  String _getPassword() {
    var now = new DateTime.now();
    String s = 'sicop.$_inteface.';

    if (XMLHeader.idMarca.isNotEmpty) {
      s += '${XMLHeader.idMarca}.';
    }

    if (XMLHeader.eMail.isNotEmpty) {
      s += '${XMLHeader.eMail}.';
    }

    if (XMLHeader.licencia.isNotEmpty) {
      s += '${XMLHeader.licencia}.';
    } else {
      s += 'null.';
    }

    if (XMLHeader.deviceId.isNotEmpty) {
      s += '${XMLHeader.deviceId}.';
    }

    s += '${now.year}.';
    s += '${now.month}.';
    s += now.day.toString();

    var bytes = utf8.encode(s);
    var digest = md5.convert(bytes);

    return digest.toString();
  }

  String getPasswordPublic() {
    var now = new DateTime.now();
    String s = 'sicop.$_inteface.';

    if (XMLHeader.idMarca.isNotEmpty) {
      s += '${XMLHeader.idMarca}.';
    }

    if (XMLHeader.eMail.isNotEmpty) {
      s += '${XMLHeader.eMail}.';
    }

    if (XMLHeader.licencia.isNotEmpty) {
      s += '${XMLHeader.licencia}.';
    } else {
      s += 'null.';
    }

    if (XMLHeader.deviceId.isNotEmpty) {
      s += '${XMLHeader.deviceId}.';
    }

    s += '${now.year}.';
    s += '${now.month}.';
    s += now.day.toString();

    var bytes = utf8.encode(s);
    var digest = md5.convert(bytes);

    return digest.toString();
  }

  void _closeTable() {
    if (_type == _TableType.row) {
      _content += '''         </row>\n''';
      _content += '''       </rows>\n''';
      _content += '''     </rows>\n''';
      _content += '''   </table>\n''';
    }

    if (_type == _TableType.filter) {
      _content += '''     </filters>\n''';
      _content += '''   </table>\n''';
    }
  }

  //MARK: ROW
  void setRowTable(String tableName) {
    _closeTable();
    _type = _TableType.row;

    _content += '''    <table>\n''';
    _content += '''      <name>$tableName</name>\n''';
    _content += '''      <filters/>\n''';
    _content += '''      <rows>\n''';
    _content += '''        <rows>\n''';
    _content += '''          <row>\n''';
  }

  void setRowArray() {
    _closeTableArray();
    _type = _TableType.row;
    _content += '''          <row>\n''';
  }

  void _closeTableArray() {
    if (_type == _TableType.row) {
      _content += '''         </row>\n''';
    }
  }

  void setRow(String parameter, String value) {
    _content += '''           <$parameter>$value</$parameter>\n''';
  }

  void setRowTelefono(String telefono, String ext, String tipo, int numT, String isFavouriteTelephone, String idpais) {
    if (telefono.isNotEmpty) {
      _content += '''           <Telefono$numT>$telefono</Telefono$numT>\n''';
      _content += '''           <TelefonoExtension$numT>$ext</TelefonoExtension$numT>\n''';
      _content += '''           <TelefonoType$numT>$tipo</TelefonoType$numT>\n''';
      _content += '''           <FavouriteTelefhone$numT>$isFavouriteTelephone</FavouriteTelefhone$numT>\n''';
      _content += '''           <IdPaisTel$numT>$idpais</IdPaisTel$numT>\n''';
    }
  }

  void setRowCorreo(String idCorreo, String correo, String tipoCorreo) {
    if (correo.isNotEmpty) {
      _content += '''           <eMail$idCorreo>$correo</eMail$idCorreo>\n''';

      switch (tipoCorreo) {
        case "0":
          _content += '''           <eMail>$correo</eMail>\n''';
          _content += '''           <eMailType$idCorreo>Particular</eMailType$idCorreo>\n''';
          break;
        case "Particular":
          _content += '''           <eMail>$correo</eMail>\n''';
          _content += '''           <eMailType$idCorreo>Particular</eMailType$idCorreo>\n''';
          break;
        case "1":
          _content += '''           <eMailOficina>$correo</eMailOficina>\n''';
          _content += '''           <eMailType$idCorreo>Oficina</eMailType$idCorreo>\n''';
          break;
        case "Oficina":
          _content += '''           <eMailOficina>$correo</eMailOficina>\n''';
          _content += '''           <eMailType$idCorreo>Oficina</eMailType$idCorreo>\n''';
          break;
        default:
          break;
      }
    }
  }

  //MARK: FILTER
  void setFilterTable(String tableName) {
    _closeTable();
    _type = _TableType.filter;

    _content += '''    <table>\n''';
    _content += '''      <name>$tableName</name>\n''';
    _content += '''      <filters>\n''';
  }

  void setFilter(String filter, String value) {
    //if (value.isNotEmpty) {
    _content += '''       <filter>\n''';
    _content += '''         <column>$filter</column>\n''';
    _content += '''         <operator>Equal</operator>\n''';
    _content += '''         <value>$value</value>\n''';
    _content += '''       </filter>\n''';
    //}
  }

  void setParser(Object parser) {
    _parser = parser;
  }

  Future<dynamic> read() async {
    dynamic objectResponse = await _callService(Webservice.read);

    return objectResponse!;
  }

  Future<dynamic> write() async {
    dynamic objectResponse = await _callService(Webservice.write);

    return objectResponse!;
  }

  void getContet() {
    debugPrint(_content, wrapWidth: 10000);
  }

  String getContentString() {
    return _content;
  }

  Future<dynamic> _callService(String url) async {
    Object? objectResponse;
    _closeTable();
    _setXMLBottom();

    debugPrint(_content, wrapWidth: 10000);
    await http.post(Uri.parse(url), body: _content, encoding: _encoding, headers: {'Content-Type': 'text/xml', 'Origen-Device': XMLHeader.origen}).then((response) {
      if (response.statusCode == 200) {
        final XmlDocument document = XmlDocument.parse(response.body);
        objectResponse = _parser.parse(document);
      } else {
        print(response.statusCode);
      }
    });

    return objectResponse!;
  }

  Future<dynamic> postDashboardScoring(String url, {Map<String, dynamic> params = const {}}) async {
    Map<String, dynamic> json = {};

    try {
      _content = (params.keys.map((key) => '$key=${Uri.encodeQueryComponent(params[key])}')).join('&');

      final Map<String, String> headers = {'Content-Type': 'application/x-www-form-urlencoded', 'Content-Length': utf8.encode(_content).length.toString()};

      print(params.keys.map((key) => '$key=${params[key]}').join('&'));

      await http.post(Uri.parse(url), body: _content, headers: headers).then((response) {
        if (response.statusCode == 200) {
          print(response.body.replaceAll('\'', ''));
          json = jsonDecode(response.body.replaceAll('\'', ''));
        } else {
          print("Ha ocurrido un error:");
          print(response.statusCode);
          json = {};
        }
      });
    } catch (_) {
      print(_.toString());
    }

    return json;
  }

  static Future<Uint8List> downloadFile(String url) {
    return http.readBytes(Uri.parse(url));
  }

  Future<dynamic> get({required String endPoint, Map<String, dynamic> parameters = const {}, parse}) async {
    JsonResponse? objectResponse ;

    try {
      String token =
          "9RLZc3bjJIjpoTR+dyXzKLJpm+zHJHZTzMwfsWj3M+ngYXkziBxZM5iUXEiGqjt4z+SqFUOiNQYfuJcwuZEasf/EYfS+MvYQF9ZeyKC/BcIISI9eo+sn6lgXPWigYcDA0pbb+4+FVnT/Tzf/AgdIR1M2/FQYd+ttd4spXQsZs+5ts0F+5PiBPRcF+GtCe0YG/p3GqPTzOTg+7z2kAktobW+spu05z127hnka61vHwqjS4PHv4kCTRYX1G7aVZt65M4UQO/9oavXPTBBBMqqZt5VZBqc4IQ3DG7uZ4aLduBhHSgsRSsbK7Aa0Kpz9q7eP5grGAZxwGO5l3LmSC9TU3kPY3ju+rvxn5VTRUJRm+iacqnJV103W2HX3SRL5LXu0";

      String url = '${Webservice.base}resources/$endPoint';
      if (parameters.isNotEmpty) {
        url += '?${parameters.keys.map((key) => '$key=${parameters[key]}').join('&')}';
      }

      final Map<String, String> headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

      debugPrint('Url -> $url');
      debugPrint('headers -> ${headers.toString().replaceAll("{", '').replaceAll("}", '')}');
      debugPrint('params -> ${parameters.keys.map((key) => '$key=${parameters[key]}').join('&').replaceAll("{", '').replaceAll("}", '')}');

      await http.get(Uri.parse(url), headers: headers).then((response) {
        print(response.statusCode);
        print(response.body.replaceAll('\'', ''));

        if (response.statusCode == 200 || response.statusCode == 201) {
          objectResponse = JsonResponse.fromJson(jsonDecode(response.body.replaceAll('\'', '')), parse);
        } else {
          objectResponse = JsonResponse(
            success: false,
            response: null,
            message: _getMesageCode(response.statusCode),
          );
        }
      });
    } catch (e) {
      debugPrint('Error al obtener peticion ${e.toString()}');
      objectResponse = JsonResponse(
        success: false,
        response: null,
        message: 'Error al obtener peticion ${e.toString()}',
      );
    }

    return objectResponse!;
  }

  Future<dynamic> post({required String endPoint, Map<String, dynamic> parameters = const {}, parse}) async {
    JsonResponse? objectResponse;

    try {
      String token =
          "9RLZc3bjJIjpoTR+dyXzKLJpm+zHJHZTzMwfsWj3M+ngYXkziBxZM5iUXEiGqjt4z+SqFUOiNQYfuJcwuZEasf/EYfS+MvYQF9ZeyKC/BcIISI9eo+sn6lgXPWigYcDA0pbb+4+FVnT/Tzf/AgdIR1M2/FQYd+ttd4spXQsZs+5ts0F+5PiBPRcF+GtCe0YG/p3GqPTzOTg+7z2kAktobW+spu05z127hnka61vHwqjS4PHv4kCTRYX1G7aVZt65M4UQO/9oavXPTBBBMqqZt5VZBqc4IQ3DG7uZ4aLduBhHSgsRSsbK7Aa0Kpz9q7eP5grGAZxwGO5l3LmSC9TU3kPY3ju+rvxn5VTRUJRm+iacqnJV103W2HX3SRL5LXu0";

      String url = '${Webservice.base}resources/$endPoint';

      final Map<String, String> headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

      debugPrint('Url -> $url');
      debugPrint('headers -> ${headers.toString().replaceAll("{", '').replaceAll("}", '')}');
      debugPrint('params -> ${parameters.toString().replaceAll("{", '').replaceAll("}", '')}');

      await http.post(Uri.parse(url), headers: headers, body: jsonEncode(parameters)).then((response) {
        print(response.statusCode);
        print(response.body.replaceAll('\'', ''));

        if (response.statusCode == 200 || response.statusCode == 201) {
          objectResponse = JsonResponse.fromJson(jsonDecode(response.body.replaceAll('\'', '')), parse);
        } else {
          objectResponse = JsonResponse(
            success: false,
            response: null,
            message: _getMesageCode(response.statusCode),
          );
        }
      });
    } catch (e) {
      debugPrint('Error al obtener peticion ${e.toString()}');
      objectResponse = JsonResponse(
        success: false,
        response: null,
        message: 'Error al obtener peticion ${e.toString()}',
      );
    }

    return objectResponse!;
  }

  Future<dynamic> put({required String endPoint, Map<String, dynamic> parameters = const {}, parse}) async {
    JsonResponse? objectResponse ;

    try {
      String token =
          "9RLZc3bjJIjpoTR+dyXzKLJpm+zHJHZTzMwfsWj3M+ngYXkziBxZM5iUXEiGqjt4z+SqFUOiNQYfuJcwuZEasf/EYfS+MvYQF9ZeyKC/BcIISI9eo+sn6lgXPWigYcDA0pbb+4+FVnT/Tzf/AgdIR1M2/FQYd+ttd4spXQsZs+5ts0F+5PiBPRcF+GtCe0YG/p3GqPTzOTg+7z2kAktobW+spu05z127hnka61vHwqjS4PHv4kCTRYX1G7aVZt65M4UQO/9oavXPTBBBMqqZt5VZBqc4IQ3DG7uZ4aLduBhHSgsRSsbK7Aa0Kpz9q7eP5grGAZxwGO5l3LmSC9TU3kPY3ju+rvxn5VTRUJRm+iacqnJV103W2HX3SRL5LXu0";

      String url = '${Webservice.base}resources/$endPoint';

      final Map<String, String> headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

      debugPrint('Url -> $url');
      debugPrint('headers -> ${headers.toString().replaceAll("{", '').replaceAll("}", '')}');
      debugPrint('params -> ${parameters.toString().replaceAll("{", '').replaceAll("}", '')}');

      await http.put(Uri.parse(url), headers: headers, body: jsonEncode(parameters)).then((response) {
        print(response.statusCode);
        print(response.body.replaceAll('\'', ''));

        if (response.statusCode == 200 || response.statusCode == 201) {
          objectResponse = JsonResponse.fromJson(jsonDecode(response.body.replaceAll('\'', '')), parse);
        } else {
          objectResponse = JsonResponse(
            success: false,
            response: null,
            message: _getMesageCode(response.statusCode),
          );
        }
      });
    } catch (e) {
      debugPrint('Error al obtener peticion ${e.toString()}');
      objectResponse = JsonResponse(
        success: false,
        response: null,
        message: 'Error al obtener peticion ${e.toString()}',
      );
    }

    return objectResponse!;
  }

  Future<dynamic> delete({required String endPoint, Map<String, dynamic> parameters = const {}, parse}) async {
    JsonResponse? objectResponse ;

    try {
      String token =
          "9RLZc3bjJIjpoTR+dyXzKLJpm+zHJHZTzMwfsWj3M+ngYXkziBxZM5iUXEiGqjt4z+SqFUOiNQYfuJcwuZEasf/EYfS+MvYQF9ZeyKC/BcIISI9eo+sn6lgXPWigYcDA0pbb+4+FVnT/Tzf/AgdIR1M2/FQYd+ttd4spXQsZs+5ts0F+5PiBPRcF+GtCe0YG/p3GqPTzOTg+7z2kAktobW+spu05z127hnka61vHwqjS4PHv4kCTRYX1G7aVZt65M4UQO/9oavXPTBBBMqqZt5VZBqc4IQ3DG7uZ4aLduBhHSgsRSsbK7Aa0Kpz9q7eP5grGAZxwGO5l3LmSC9TU3kPY3ju+rvxn5VTRUJRm+iacqnJV103W2HX3SRL5LXu0";

      String url = '${Webservice.base}resources/$endPoint';

      final Map<String, String> headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

      debugPrint('Url -> $url');
      debugPrint('headers -> ${headers.toString().replaceAll("{", '').replaceAll("}", '')}');
      debugPrint('params -> ${parameters.toString().replaceAll("{", '').replaceAll("}", '')}');

      await http.put(Uri.parse(url), headers: headers, body: jsonEncode(parameters)).then((response) {
        print(response.statusCode);
        print(response.body.replaceAll('\'', ''));

        if (response.statusCode == 200 || response.statusCode == 201) {
          objectResponse = JsonResponse.fromJson(jsonDecode(response.body.replaceAll('\'', '')), parse);
        } else {
          objectResponse = JsonResponse(
            success: false,
            response: null,
            message: _getMesageCode(response.statusCode),
          );
        }
      });
    } catch (e) {
      debugPrint('Error al obtener peticion ${e.toString()}');
      objectResponse = JsonResponse(
        success: false,
        response: null,
        message: 'Error al obtener peticion ${e.toString()}',
      );
    }

    return objectResponse!;
  }

  String _getMesageCode(int code) {
    String s = "";

    switch (code) {
      case 200:
        {
          s = "La solicitud ha tenido éxito";
        }
        break;
      case 201:
        {
          s = "La solicitud ha tenido éxito y se ha creado un nuevo recurso como resultado de ello.";
        }
        break;
      case 302:
        {
          s = "Recurso ubicado temporalmente en otro lugar según el encabezado.";
        }
        break;
      case 303:
        {
          s = "La solicitud condicional habría tenido éxito, pero la condición era falsa, por lo que no se envió ningún cuerpo.";
        }
        break;
      case 401:
        {
          s = "Usuario no autorizado";
        }
        break;
      case 400:
        {
          s = "Solicitud incorrecta.";
        }
        break;
      case 403:
        {
          s = "Permiso no autorizado.";
        }
        break;
      case 404:
        {
          s = "No encontrado.";
        }
        break;
      case 500:
        {
          s = "Error interno del servidor.";
        }
        break;
      case 502:
        {
          s = "Puerta de enlace incorrecto.";
        }
        break;
      case 503:
        {
          s = "Servidor no disponible.";
        }
        break;
      case 504:
        {
          s = "Tiempo de espera agotado.";
        }
        break;

      default:
        {
          s = "";
        }
    }

    return s;
  }
}
