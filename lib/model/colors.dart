import 'package:flutter/material.dart';

class SOColors {
  static const blue = Color(0xFF092C81);
  static const green = Color(0xFF61CE70);
  static const background = Color(0xFFF3F3F3);
  static const yellow = Color(0xFFF1C40E);
  static const red = Color(0xFFE74C3C);
  static const gray = Color(0xFFAFBDC3);
}

class HexToColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    else {
      hexColor = "FFFFFFFF";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexToColor(final String hexColor) : super(_getColorFromHex(hexColor));
}