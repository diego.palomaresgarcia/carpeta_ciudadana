import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:ui';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../model/constants.dart';
import 'package:device_info_plus/device_info_plus.dart';
import '../model/routes.dart';
import '../model/sesion.dart';
import 'dart:io';
import '../model/colors.dart';
import 'package:flutter/cupertino.dart';

void main() {
  runApp(new Carpeta_Ciudadana());
}

class Carpeta_Ciudadana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      systemNavigationBarColor: SOColors.blue,
      statusBarColor: SOColors.background,
    ));

    Webservice.setup();
    _saveXMLHeader();

    return GetMaterialApp(
      title: 'Carpeta Ciudadana',
      debugShowCheckedModeBanner: false,
      // theme: ThemeData(
      //   primaryColor: const Color(0xFFFFFFFF),
      //   secondaryHeaderColor: Colors.black45,
      //   canvasColor: SOColors.background,
      //   scaffoldBackgroundColor: SOColors.background,
      //   // buttonColor: SOColors.blue,
      //   // cursorColor: Colors.black45,
      //   primarySwatch: Colors.grey,
      //   appBarTheme: const AppBarTheme(elevation: 0.5),
      //   buttonTheme: const ButtonThemeData(
      //     textTheme: ButtonTextTheme.primary,
      //     height: 40.0,
      //     minWidth: 200.0,
      //   ),
      //   fontFamily: 'VarelaRound',
      //   primaryTextTheme: const TextTheme(
      //   //   // displayLarge ,
      //   //   // displayMedium ,
      //   //   // displaySmall ,
      //   //   // headlineMedium,
      //   //   // headlineSmall ,
      //   //   // titleLarge: const TextStyle(fontSize: 18.0, fontFamily: 'VarelaRound', fontWeight: FontWeight.w600),
      //   //   // titleMedium: const TextStyle(fontSize: 18.0, fontFamily: 'VarelaRound', fontWeight: FontWeight.w600),
      //   //   // titleSmall: const TextStyle(fontSize: 18.0, fontFamily: 'VarelaRound', fontWeight: FontWeight.w600),
      //   //   // bodyLarge ,
      //   //   // bodyMedium ,
      //   //   // bodySmall ,
      //   //   // labelLarge ,
      //   //   // labelSmall ,
      //     headline1: TextStyle(
      //       fontSize: 18.0,
      //       fontFamily: 'VarelaRound',
      //       fontStyle: FontStyle.italic,
      //       color: Color(0xFF434343),
      //     ),
      //     headline2: TextStyle(
      //       fontSize: 17.0,
      //       fontFamily: 'VarelaRound',
      //       color: Color(0xFF434343),
      //     ),
      //   //   // headline3 ,
      //   //   // headline4,
      //   //   // headline5 ,
      //   //   // headline6 ,
      //     subtitle1: TextStyle(
      //       fontSize: 16.0,
      //       fontFamily: 'VarelaRound',
      //       color: Color(0xFF2E2E2E),
      //     ),
      //     subtitle2: TextStyle(
      //       fontSize: 16.0,
      //       fontFamily: 'VarelaRound',
      //       color: Color(0xFF2E2E2E),
      //     ),
      //     bodyText1: TextStyle(
      //       fontSize: 16.0,
      //       fontFamily: 'VarelaRound',
      //       color: Color(0xFF585858),
      //     ),
      //     bodyText2: TextStyle(
      //       fontSize: 14.0,
      //       fontFamily: 'VarelaRound',
      //       color: Color(0xFF838383),
      //     ),
      //   //   // caption,
      //   //   // button,
      //   //   // overline,
      //   ),
      //   inputDecorationTheme: const InputDecorationTheme(
      //     disabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
      //     labelStyle: TextStyle(fontFamily: 'SFProLight', color: Colors.black45),
      //     focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: SOColors.blue, width: 1.5)),
      //   ),
      //   textTheme: TextTheme(
      //       titleMedium: const TextStyle(fontSize: 18.0, fontFamily: 'SFProMedium', fontWeight: FontWeight.w600),
      //       subtitle1: const TextStyle(fontSize: 15.0, fontFamily: 'SFProMedium', color: Colors.black
      //           ),
      //       bodyText1: const TextStyle(
      //         fontSize: 14.0,
      //         fontFamily: 'SFProMedium',
      //         color: Color(0xFF434343),
      //       ),
      //       headline1: const TextStyle(
      //         fontSize: 17.0,
      //         fontFamily: 'SFProHeavy',
      //         fontStyle: FontStyle.italic,
      //         color: Color(0xFF434343),
      //       )),
      //   sliderTheme: SliderThemeData(
      //     valueIndicatorTextStyle: Theme.of(context).primaryTextTheme.subtitle1?.copyWith(color: Colors.white),
      //   ),
      // ),
      routes: routes,
      //home: SplashPage(),
    );
  }

  void _saveXMLHeader() async {
    // XMLHeader.deviceId = await _getDeviceId();
    // XMLHeader.idMarca = await Session.get(SessionData.idMarca);
    // XMLHeader.eMail = await Session.get(SessionData.eMail);
    // XMLHeader.idDistribuidor = await Session.get(SessionData.idDistribuidor);
    // XMLHeader.poolName = await Session.get(SessionData.poolName);
    // XMLHeader.licencia = await Session.get(SessionData.licencia);
    // XMLHeader.origen = Platform.isIOS ? "3" : "2";
  }

  Future<String> _getDeviceId() async {
    try {
      DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();

      if (Platform.isAndroid) {
        var deviceData = await deviceInfo.androidInfo;
        return deviceData.id.toString().replaceAll("-", "");
      } else if (Platform.isIOS) {
        var deviceData = await deviceInfo.iosInfo;
        return deviceData.identifierForVendor.toString().replaceAll("-", "");
      }
    } catch (id, id2) {
      print(id);
      print(id2);
    }
    return "unknow";
  }
}
