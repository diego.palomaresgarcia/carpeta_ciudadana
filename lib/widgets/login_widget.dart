import 'package:flutter/material.dart';

import '../model/colors.dart';

class LoginBG extends StatelessWidget {

   Widget child;

  LoginBG({
     required this.child,
     Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: key,
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  child: Container(
                    color: Theme.of(context).canvasColor,
                  ),
                ),
                Expanded(
                  child: CustomPaint(
                    painter: _LoginShape(),
                    child: Container(),
                  ),
                )
              ],
            ),
            Scaffold(
                backgroundColor: Colors.transparent,
                body: child
            )
          ],
        )
    );
  }
}

class _LoginShape extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final Gradient gradient = LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      stops: [
        0.25,
        0.9
      ],
      colors: [SOColors.blue, SOColors.green],
      tileMode: TileMode.clamp,
    );

    final Rect colorBounds = Rect.fromLTRB(0, 0, size.width, size.height);
    final Paint paint = new Paint()
      ..shader = gradient.createShader(colorBounds);

    Path path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, size.height * 0.2);
    path.lineTo(0, 0);
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}