import 'package:get/get.dart';
import 'package:flutter/material.dart';
import '../../model/colors.dart';
import '../../controllers/splash/splash_controller.dart';

class SplashPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return GetBuilder(
      init: SplashController(),
      builder: (_) => Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.1,
                0.8
              ],
              colors: [SOColors.blue, SOColors.green],
              tileMode: TileMode.clamp,
            )
        ),
        child: Center(
          child: Image.asset(
            'assets/images/logo.png' ,
            height: 200.0,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}