import 'package:get/get.dart';
import 'package:flutter/material.dart';

import '../../controllers/login/login_controller.dart';
import '../../widgets/login_widget.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
      id: 'vista',
      init: LoginController(),
      builder: (_) => Scaffold(
        body:LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
          return  Center(
            child: ListView(
              shrinkWrap: true,
              physics: const ScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: (constraints.maxWidth <= 400.0 ? constraints.maxWidth -  320 / 2 : (constraints.maxWidth > 400.0 ? (constraints.maxWidth -  400) * 0.5 : 25))),
              children: [
                Card(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: Form(
                      key: _.formKey,
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 8,
                          ),
                          Text('Carpeta Ciudadana', style: Theme.of(context).textTheme.headline6, maxLines: 1),
                          const SizedBox(
                            height: 8,
                          ),
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: const InputDecoration(
                                prefixIcon: Icon(
                                  Icons.person,
                                ),
                                labelText: 'Curp',
                                filled: true),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          GetBuilder<LoginController>(
                            id: 'password',
                            builder: (_) => TextFormField(
                              obscureText: true,
                              decoration: InputDecoration(
                                filled: _.obscureText,
                                prefixIcon: const Icon(
                                  Icons.lock,
                                ),
                                labelText: 'Contraseña',
                                suffixIcon: IconButton(
                                  icon: _.obscureText ? const Icon(Icons.visibility_off) : const Icon(Icons.visibility),
                                  onPressed: _.changePasswordVisibility,
                                  // color: Theme.of(context).secondaryHeaderColor
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(4),
                            child: Stack(
                              children: <Widget>[
                                Positioned.fill(
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      gradient: LinearGradient(
                                        colors: <Color>[
                                          Color(0xFF0D47A1),
                                          Color(0xFF1976D2),
                                          Color(0xFF42A5F5),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                TextButton(
                                  style: TextButton.styleFrom(
                                    padding: const EdgeInsets.all(16.0),
                                    primary: Colors.white,
                                    textStyle: const TextStyle(fontSize: 20),
                                  ),
                                  onPressed: _.verifyLogin,
                                  child: const Text('Ingresar'),
                                ),
                              ],
                            ),
                          ),
                          TextButton(
                            child: const Text('¿Olvidaste tu contraseña?'),
                            onPressed: () {},
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },),
      ),
    );
  }
}
